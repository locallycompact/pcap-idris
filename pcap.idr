module Main

import CFFI
import Data.Buffer
import Prelude.Strings

%include C "pcap/pcap.h"
%lib C "pcap"

pcap_create : String -> String -> IO Ptr
pcap_create x y = foreign FFI_C "pcap_create" (String -> String -> IO Ptr) x y

pcap_activate : Ptr -> IO Int
pcap_activate x = foreign FFI_C "pcap_activate" (Ptr -> IO Int) x

pcap_statustostr : Int -> IO String
pcap_statustostr x = foreign FFI_C "pcap_statustostr" (Int -> IO String) x

pcap_major_version : Ptr -> IO Int
pcap_major_version = foreign FFI_C "pcap_major_version" (Ptr -> IO Int)

pcap_next : Ptr -> Ptr -> IO String
pcap_next = foreign FFI_C "pcap_next" (Ptr -> Ptr -> IO String)

pcap_next_ex : Ptr -> Ptr -> Ptr -> IO Int
pcap_next_ex = foreign FFI_C "pcap_next_ex" (Ptr -> Ptr -> Ptr -> IO Int)

main : IO ()
main = do x <- pcap_create "any" ""
          y <- pcap_activate x
          putStrLn . show $ y
          z <- pcap_statustostr y
          putStrLn z
          t <- pcap_major_version x
          withAlloc PTR $ \p =>
            withAlloc PTR $ \q => do
            d <- pcap_next_ex x p q
            f <- peek PTR q
            if f == null
               then print "nay" else do
                 k <- getStringFromBuffer (MkString f)
                 putStrLn k
