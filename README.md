    nix-shell -p gnumake -p gmp -p libpcap -p glibc
    stack exec --nix-packages libpcap  --resolver lts-11.14 -- idris --cg-opt '-D_GNU_SOURCE' --package contrib pcap.idr -o pcap
