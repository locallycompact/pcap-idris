DEFAULT: pcap.idr
	idris --cg-opt '-D_GNU_SOURCE' pcap.idr -o pcap

clean:
	rm -f pcap pcap.ibc
